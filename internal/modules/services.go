package modules

import (
	"gitlab.com/Vallyenfail/grpc-user/internal/infrastructure/component"
	uservice "gitlab.com/Vallyenfail/grpc-user/internal/modules/user/service"
	"gitlab.com/Vallyenfail/grpc-user/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
