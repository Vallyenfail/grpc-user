package handlers

import (
	"gitlab.com/Vallyenfail/grpc-user/internal/infrastructure/errors"
	"gitlab.com/Vallyenfail/grpc-user/internal/infrastructure/middleware"
	"gitlab.com/Vallyenfail/grpc-user/internal/infrastructure/tools/cryptography"
	"net/http"
	"strconv"
)

func ExtractUser(r *http.Request) (cryptography.UserFromClaims, error) {
	ctx := r.Context()
	u, ok := ctx.Value(middleware.UserRequest{}).(cryptography.UserClaims)
	if !ok {
		return cryptography.UserFromClaims{}, errors.TokenExtractUserError
	}
	userID, err := strconv.Atoi(u.ID)
	if err != nil {
		return cryptography.UserFromClaims{}, errors.TokenExtractUserError
	}
	role, err := strconv.Atoi(u.Role)
	if err != nil {
		return cryptography.UserFromClaims{}, errors.TokenExtractUserError
	}

	return cryptography.UserFromClaims{
		ID:     userID,
		Role:   role,
		Groups: nil,
		Layers: nil,
	}, nil
}
